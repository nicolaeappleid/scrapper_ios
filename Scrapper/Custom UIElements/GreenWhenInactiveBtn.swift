//
//  GreenWhenInactiveBtn.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 08/04/2021.
//

import UIKit

class GreenWhenInactiveBtn: UIButton {
    
    override var isEnabled: Bool {
        didSet {
            updateUI()
        }
        
    }
    
    func updateUI() {
        let btnColor = isEnabled ? #colorLiteral(red: 0.4274509804, green: 0, blue: 0.9647058824, alpha: 1) : #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        let btnTitle = isEnabled ? title(for: .normal) : "DONE"
     
        backgroundColor = btnColor
        setTitle(btnTitle, for: .normal)
    }
    
    
}
