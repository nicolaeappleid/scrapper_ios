//
//  FacebookURLProtocol.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 08/04/2021.
//
import Foundation

// MARK: URLProtocol
class CustomURLProtocol: URLProtocol, URLSessionDataDelegate, URLSessionTaskDelegate {
    private var dataTask: URLSessionDataTask?
    private var urlResponse: URLResponse?
    private var receivedData: NSMutableData?
    
    
    override class func canInit(with request: URLRequest) -> Bool {
        print("Request URL = \(request.url?.absoluteString)")
        print(request.allHTTPHeaderFields)

        guard
            let url = request.url,
            let headers = request.allHTTPHeaderFields // [String : String]
        else { return false }
        
//        print("Request URL = \(url.absoluteString)")
        if url.absoluteString.contains("facebook") {
            let urlInfo: [String: String] = ["url": url.absoluteString]
            NotificationCenter.default.post(name: .fbURLAccessed, object: self, userInfo: urlInfo)
        }
        
        if url.absoluteString.contains("amazon") {
            let urlInfo: [String: String] = ["url": url.absoluteString]
            NotificationCenter.default.post(name: .amzURLAccessed, object: self, userInfo: urlInfo)
        }
        
        if url.absoluteString.contains("youtube"),
           let refearerURL = headers.first(where: {$0.key == "Referer"}) {
            let urlInfo: [String: String] = ["url": url.absoluteString, "referer": refearerURL.value]
            
            NotificationCenter.default.post(name: .ytURLAccessed, object: self, userInfo: urlInfo)
        }
        
        if url.absoluteString.contains("twitter"),
           let refearerURL = headers.first(where: {$0.key == "Referer"}) {
            let urlInfo: [String: String] = ["url": url.absoluteString, "referer": refearerURL.value]

            NotificationCenter.default.post(name: .twtURLAccessed, object: self, userInfo: urlInfo)
        }
        
        if url.absoluteString.contains("instagram"),
           let refearerURL = headers.first(where: {$0.key == "Referer"}) {
            let urlInfo: [String: String] = ["url": url.absoluteString, "referer": refearerURL.value]

            NotificationCenter.default.post(name: .instURLAccessed, object: self, userInfo: urlInfo)
        }
        return false
    }
    
}
