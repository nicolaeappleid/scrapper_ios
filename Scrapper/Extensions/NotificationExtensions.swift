//
//  NotificationExtensions.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 08/04/2021.
//

import Foundation

extension Notification.Name {
    static let fbURLAccessed = Notification.Name("fbURLAccessed")
    static let amzURLAccessed = Notification.Name("amzURLAccessed")
    static let ytURLAccessed = Notification.Name("ytURLAccessed")
    static let twtURLAccessed = Notification.Name("twtURLAccessed")
    static let instURLAccessed = Notification.Name("instURLAccessed")
}
