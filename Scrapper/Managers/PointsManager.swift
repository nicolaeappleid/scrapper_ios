//
//  PointsManager.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 07/04/2021.
//

import UIKit

class PointsManager {
    
    static let shared = PointsManager()
    
    //Initializer access level change now
    private init(){}
    
    var totalPoints: Int = 0
    
    private var adPoints: Int = 5
    private var trackSitePoints: Int = 5
    private var amzPoints: Int = 5
    private var fbLikePoints: Int = 10
    private var ytLikePoints: Int = 10
    private var twtLikePoints: Int = 10
    private var instLikePoints: Int = 10
    private var appInstallPoints: Int = 10
    private var trackAppPoints: Int = 10
    
    var adWasTapped = false {
        didSet {
            if adWasTapped {
                totalPoints += adPoints
            }
        }
    }
    
    var trackSiteComplete = false {
        didSet {
            if trackSiteComplete {
                totalPoints += trackSitePoints
            }
        }
    }
    
    var amzReviewComplete = false {
        didSet {
            if amzReviewComplete {
                totalPoints += amzPoints
            }
        }
    }
    var fbLikeComplete = false {
        didSet {
            if fbLikeComplete {
                totalPoints += fbLikePoints
            }
        }
    }
    var ytLikeComplete = false {
        didSet {
            if ytLikeComplete {
                totalPoints += ytLikePoints
            }
        }
    }
    var twtLikeComplete = false {
        didSet {
            if twtLikeComplete {
                totalPoints += twtLikePoints
            }
        }
    }
    var instLikeComplete = false {
        didSet {
            if instLikeComplete {
                totalPoints += instLikePoints
            }
        }
    }
    var appInstallComplete = false {
        didSet {
            if appInstallComplete {
                totalPoints += appInstallPoints
            }
        }
    }
    var trackAppComplete = false {
        didSet {
            if trackAppComplete {
                totalPoints += trackAppPoints
            }
        }
    }
    
}
