//
//  AmazonWebViewController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 07/04/2021.
//

import UIKit
import WebKit

class AmazonWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var baseURLString = "https://www.amazon"
    var urlPath = "/review/create-review?asin="
    var thankReviewUrlPath = "/review-your-purchases/api/v1/reviews/edit-review?"
    var domain: Domain = .COM
    var productCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        setupWebView()
        
        let urlToLoad = baseURLString + domain.rawValue + urlPath + productCode
        loadLink(urlString: urlToLoad)
        
    }

    private func setupWebView() {
        webView.delegate = self
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveAmazonUrl(_:)), name: .amzURLAccessed, object: nil)
    }
    
    private func loadLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        webView.loadRequest(URLRequest(url: url))
    }
    
    @objc func didReceiveAmazonUrl(_ notification: Notification) {
        if let data = notification.userInfo as? [String: String] {
            for (name, value) in data {
                print("\(name) - \(value)")
                guard let url = URL(string: value) else { return }
                if url.absoluteString.contains(thankReviewUrlPath) {
                    thanksForAction(with: "Thank you for the review!")
                } else {
                    print("User Stoped leaving site: \(url.absoluteString)")
                }
                
            }
        }
    }
    
    private func thanksForAction(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.destructive, handler: { action in
                PointsManager.shared.amzReviewComplete = true
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}

//MARK:- UIWebViewDelegate Methods
extension AmazonWebViewController: UIWebViewDelegate {
    
   
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = webView.request?.url else { return true }
        print("shouldStartLoadWith - url: \(url.absoluteString)")
        
        guard url.path != "" else { return true }
        if url.absoluteString.hasPrefix(baseURLString) {
            return true
        } else {
            print("User Stoped leaving site: \(url.absoluteString)")
            return false
        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading { return }
        guard let url = webView.request?.url else { return }
        print("didFinish - webView.url: \(String(describing: url.absoluteString))")
    }
}
