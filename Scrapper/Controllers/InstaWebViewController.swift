//
//  InstaWebViewController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 12/04/2021.
//

import UIKit

class InstaWebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    

    var baseURLString = "https://www.instagram.com/"
    var urlPath = "p/B4hm4Dug0DU/"
    
    var comBaseURLString = "/web/comments/2171187473428988116/add/"
    var likeBaseURLString = "/web/likes/2171187473428988116/like/"

//    2171187473428988116 //  specific for this post only

    //https://www.instagram.com/p/B4hm4Dug0DU/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        setupWebView()
        let urlToLoad = baseURLString + urlPath
        loadLink(urlString: urlToLoad)
    }

    private func setupWebView() {
        webView.delegate = self
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveInstUrl(_:)), name: .instURLAccessed, object: nil)
    }
    
    private func loadLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        webView.loadRequest(URLRequest(url: url))
    }
    
    @objc func didReceiveInstUrl(_ notification: Notification) {
        if let data = notification.userInfo as? [String: String] {
            guard
                let urlString = data.first(where: {$0.key == "url"})?.value,
                let referer = data.first(where: {$0.key == "referer"})?.value,
                let url = URL(string: urlString)
            else { return }
            
            if referer.contains(urlPath),
                url.absoluteString.hasPrefix(baseURLString) {
                
                    if url.absoluteString.contains(likeBaseURLString) {
                        thanksForAction(with: "Thank you for the Like!")
                    }
                    if url.absoluteString.contains(comBaseURLString) {
                        thanksForAction(with: "Thank you for the Comment!")
                    }
                } else {
                    print("No action for this url: \(url.absoluteString)")
                }
                
            
        }
    }
    
    private func thanksForAction(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.destructive, handler: { action in
                PointsManager.shared.instLikeComplete = true
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

//MARK:- UIWebViewDelegate Methods
extension InstaWebViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = webView.request?.url else { return true }
        print("shouldStartLoadWith - url: \(url.absoluteString)")
        
//        guard url.path != "" else { return true }
//        if url.absoluteString.hasPrefix(mBaseURLString) || url.absoluteString.hasPrefix(baseURLString) {
            return true
//        } else {
//            print("User Stoped leaving site: \(url.absoluteString)")
//            return false
//        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading { return }
        guard let url = webView.request?.url else { return }
        print("didFinish - webView.url: \(String(describing: url.absoluteString))")
    }

}
