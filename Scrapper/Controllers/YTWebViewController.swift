//
//  YTWebViewController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 09/04/2021.
//

import UIKit

class YTWebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    

    
    var baseURLString = "https://www.youtube.com/"
    var mBaseURLString = "https://m.youtube.com/"
    var urlPath = "watch?v="
    var ytVideoID = "dQw4w9WgXcQ"
    
    var comBaseURLString = "/youtubei/v1/comment/create_comment"
    var likeBaseURLString = "/youtubei/v1/like/like"
    
//    https://m.youtube.com/youtubei/v1/like/removelike // remove like
//    https://m.youtube.com/youtubei/v1/like/like // add like
//    https://m.youtube.com/youtubei/v1/comment/create_comment // add comment
//    https://m.youtube.com/youtubei/v1/comment/perform_comment_action // like/ dislike a comment
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        setupWebView()
        let urlToLoad = baseURLString + urlPath + ytVideoID
        loadLink(urlString: urlToLoad)
    }

    private func setupWebView() {
        webView.delegate = self
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveYTUrl(_:)), name: .ytURLAccessed, object: nil)
    }
    
    private func loadLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        webView.loadRequest(URLRequest(url: url))
    }
    
    @objc func didReceiveYTUrl(_ notification: Notification) {
        if let data = notification.userInfo as? [String: String] {

            
            guard
                let urlString = data.first(where: {$0.key == "url"})?.value,
                let referer = data.first(where: {$0.key == "referer"})?.value,
                let url = URL(string: urlString)
            else { return }
            
            if referer.contains(urlPath + ytVideoID),
               url.absoluteString.hasPrefix(mBaseURLString) ||
                url.absoluteString.hasPrefix(baseURLString) {
                    if url.absoluteString.contains(likeBaseURLString) {
                        thanksForAction(with: "Thank you for the Like!")
                    }
                    if url.absoluteString.contains(comBaseURLString) {
                        thanksForAction(with: "Thank you for the Comment!")
                    }
                } else {
                    print("User Stoped leaving site: \(url.absoluteString)")
                }
                
            
        }
    }
    
    private func thanksForAction(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.destructive, handler: { action in
                PointsManager.shared.ytLikeComplete = true
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

//MARK:- UIWebViewDelegate Methods
extension YTWebViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = webView.request?.url else { return true }
        print("shouldStartLoadWith - url: \(url.absoluteString)")
        
//        guard url.path != "" else { return true }
//        if url.absoluteString.hasPrefix(mBaseURLString) || url.absoluteString.hasPrefix(baseURLString) {
            return true
//        } else {
//            print("User Stoped leaving site: \(url.absoluteString)")
//            return false
//        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading { return }
        guard let url = webView.request?.url else { return }
        print("didFinish - webView.url: \(String(describing: url.absoluteString))")
    }

}

