//
//  TwtWebViewController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 09/04/2021.
//

import UIKit

class TwtWebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    

    var baseURLString = "https://www.twitter.com/"
    var mBaseURLString = "https://mobile.twitter.com/"
    var urlPath = "elonmusk/status/1380480918243090433?s=20"
    
    var comBaseURLString = "/youtubei/v1/comment/create_comment"
    var likeBaseURLString = "/i/api/1.1/favorites/create.json"
    //Referer
// comment //   https://api.twitter.com/live_pipeline/events?topic=%2Ftweet_engagement%2F1380480918243090433
    // retweet // https://mobile.twitter.com/i/api/1.1/statuses/retweet.json

    //    https://twitter.com/elonmusk/status/1380480918243090433?s=20
    //    https://twitter.com/elonmusk/status/1372688191803768840?s=20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        setupWebView()
        let urlToLoad = baseURLString + urlPath
        loadLink(urlString: urlToLoad)
    }

    private func setupWebView() {
        webView.delegate = self
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveTWTUrl(_:)), name: .twtURLAccessed, object: nil)
    }
    
    private func loadLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        webView.loadRequest(URLRequest(url: url))
    }
    
    @objc func didReceiveTWTUrl(_ notification: Notification) {
        if let data = notification.userInfo as? [String: String] {
            guard
                let urlString = data.first(where: {$0.key == "url"})?.value,
                let referer = data.first(where: {$0.key == "referer"})?.value,
                let url = URL(string: urlString)
            else { return }
            
            if referer.contains(urlPath),
               url.absoluteString.hasPrefix(mBaseURLString) ||
                url.absoluteString.hasPrefix(baseURLString) {
                
                    if url.absoluteString.contains(likeBaseURLString) {
                        thanksForAction(with: "Thank you for the Like!")
                    }
                    if url.absoluteString.contains(comBaseURLString) {
                        thanksForAction(with: "Thank you for the Comment!")
                    }
                } else {
                    print("No action for this url: \(url.absoluteString)")
                }
                
            
        }
    }
    
    private func thanksForAction(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.destructive, handler: { action in
                PointsManager.shared.twtLikeComplete = true
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

//MARK:- UIWebViewDelegate Methods
extension TwtWebViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = webView.request?.url else { return true }
        print("shouldStartLoadWith - url: \(url.absoluteString)")
        
//        guard url.path != "" else { return true }
//        if url.absoluteString.hasPrefix(mBaseURLString) || url.absoluteString.hasPrefix(baseURLString) {
            return true
//        } else {
//            print("User Stoped leaving site: \(url.absoluteString)")
//            return false
//        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading { return }
        guard let url = webView.request?.url else { return }
        print("didFinish - webView.url: \(String(describing: url.absoluteString))")
    }

}
