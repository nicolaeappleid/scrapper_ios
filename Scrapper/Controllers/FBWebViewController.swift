//
//  FBWebViewController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 07/04/2021.
//

import UIKit
import WebKit

class FBWebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    var baseURLString = "https://www.facebook.com/"
    var mBaseURLString = "https://m.facebook.com/"
    var urlPath = "facebook/photos/a.108824087345859/200337158194551/"
    
    var comBaseURLString = "https://m.facebook.com/a/comment.php?fs"
    var likeBaseURLString = "https://m.facebook.com/ufi/reaction"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        setupWebView()
        let urlToLoad = baseURLString + urlPath
        loadLink(urlString: urlToLoad)
    }

    private func setupWebView() {
        webView.delegate = self
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveFBUrl(_:)), name: .fbURLAccessed, object: nil)
    }
    
    private func loadLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        webView.loadRequest(URLRequest(url: url))
    }
    
    @objc func didReceiveFBUrl(_ notification: Notification) {
        if let data = notification.userInfo as? [String: String] {
            for (name, value) in data {
                print("\(name) - \(value)")
                guard let url = URL(string: value) else { return }
                if url.absoluteString.hasPrefix(mBaseURLString) || url.absoluteString.hasPrefix(baseURLString) {
                    if url.absoluteString.contains(likeBaseURLString) {
                        thanksForAction(with: "Thank you for the Like!")
                    }
                    if url.absoluteString.contains(comBaseURLString) {
                        thanksForAction(with: "Thank you for the Comment!")
                    }
                } else {
                    print("User Stoped leaving site: \(url.absoluteString)")
                }
                
            }
        }
    }
    
    private func thanksForAction(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.destructive, handler: { action in
                PointsManager.shared.fbLikeComplete = true
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

//MARK:- UIWebViewDelegate Methods
extension FBWebViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = webView.request?.url else { return true }
        print("shouldStartLoadWith - url: \(url.absoluteString)")
        
        guard url.path != "" else { return true }
        if url.absoluteString.hasPrefix(mBaseURLString) || url.absoluteString.hasPrefix(baseURLString) {
            return true
        } else {
            print("User Stoped leaving site: \(url.absoluteString)")
            return false
        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading { return }
        guard let url = webView.request?.url else { return }
        print("didFinish - webView.url: \(String(describing: url.absoluteString))")
    }

}
