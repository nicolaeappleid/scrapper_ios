//
//  MainController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 07/04/2021.
//

import UIKit

enum Domain: String {
    case DE = ".de"
    case CO_UK = ".co.uk"
    case COM = ".com"
}

// AMAZON PRODUCT :  B07FSKRZNC
class MainController: UIViewController {
    
    @IBOutlet var roundCornerBtns: [UIButton]!
    @IBOutlet weak var domainSegmentedView: UISegmentedControl!
    @IBOutlet weak var amazonLabel: UILabel!
    @IBOutlet weak var trackPageLabel: UIView!
    @IBOutlet weak var facebookLabel: UILabel!
    @IBOutlet weak var installLokiLabel: UILabel!
    @IBOutlet weak var useLokiLabel: UIView!
    @IBOutlet weak var clockOnAdLabel: UILabel!
    @IBOutlet weak var totalScoreLabel: UILabel!
    
    @IBOutlet weak var amazonTextField: UITextField!
    
    @IBOutlet weak var amazonBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var trackSiteBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var fbBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var installAppBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var trackAppBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var adBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var ytBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var twtBtn: GreenWhenInactiveBtn!
    @IBOutlet weak var instBtn: GreenWhenInactiveBtn!
    
    var selectedDomain: Domain = .DE
    var amazonProduct = "B07FSKRZNC"
    let appLinkInAppStore = "itms-apps://apple.com/app/id1501207011"
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }

    private func setup() {
        title = "Scrapper"
        amazonTextField.delegate = self
        amazonTextField.text = amazonProduct
        
        domainSegmentedView.selectedSegmentIndex = 0
        self.hideKeyboardWhenTappedAround()
        setBtnsCornerRadius()
    }
    
    private func updateUI() {
        totalScoreLabel.text = "Total Score: \(PointsManager.shared.totalPoints)"
        amazonBtn.isEnabled = !PointsManager.shared.amzReviewComplete
        trackSiteBtn.isEnabled = !PointsManager.shared.trackSiteComplete
        fbBtn.isEnabled = !PointsManager.shared.fbLikeComplete
        installAppBtn.isEnabled = !PointsManager.shared.appInstallComplete
        trackAppBtn.isEnabled = !PointsManager.shared.trackAppComplete
        adBtn.isEnabled = !PointsManager.shared.adWasTapped
        ytBtn.isEnabled = !PointsManager.shared.ytLikeComplete
        twtBtn.isEnabled = !PointsManager.shared.twtLikeComplete
        instBtn.isEnabled = !PointsManager.shared.instLikeComplete
    }

    private func setupKeyboardStuff() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    private func setBtnsCornerRadius() {
        roundCornerBtns.forEach { (btn) in
            btn.layer.cornerRadius = 6
            btn.layer.shadowColor = UIColor.black.cgColor
            btn.layer.shadowRadius = 4
            btn.layer.shadowOpacity = 0.2
            btn.layer.shadowOffset = CGSize(width: 0, height: 2)
        }
        
        
    }
    
    private func checkIfInstalled(appBundle: String) {
        let app = UIApplication.shared
        let bundleID = "\(appBundle)://"
        guard let appUrl = URL(string: bundleID) else { return }
        
        if app.canOpenURL(appUrl) {
            print("App is installed")
            PointsManager.shared.appInstallComplete = true
            updateUI()
        } else {
           print("App in not installed. Go to AppStore")
            let alert = UIAlertController(title: "App not found.", message: "Would you like to download it?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Download App", style: .default, handler: { action in
                self.goToAppStore(appLink: self.appLinkInAppStore)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    private func goToAppStore(appLink: String) {
        if let url = URL(string: appLink) {
            UIApplication.shared.open(url)
        }
    }

    @IBAction func didTapTrackBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "TrackWebViewController") as? TrackWebViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func didTapAddButton(_ sender: Any) {
        PointsManager.shared.adWasTapped = true
        updateUI()
    }
    
    @IBAction func didTapVerifyAppBtn(_ sender: Any) {
        checkIfInstalled(appBundle: "com.adonis.payperminute")
    }
    
    @IBAction func didTapStoreBtn(_ sender: Any) {
        goToAppStore(appLink: appLinkInAppStore)
    }
    
    @IBAction func didTapAmazon(_ sender: Any) {
        amazonProduct = amazonTextField.text ?? amazonProduct
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "AmazonWebViewController") as? AmazonWebViewController else { return }
        vc.domain = selectedDomain
        vc.productCode = amazonProduct
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapFacebookBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "FBWebViewController") as? FBWebViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func indexChanged(sender: UISegmentedControl) {
          switch domainSegmentedView.selectedSegmentIndex
          {
          case 0:
            selectedDomain = .DE
          case 1:
            selectedDomain = .CO_UK
          case 2:
            selectedDomain = .COM
          default:
              break;
          }
      }
    
    @IBAction func didTapAppSpendTimeBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Feature unavailable.", message: "This feature is not available on iOS devices at the moment.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapYoutubeBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "YTWebViewController") as? YTWebViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapTwitterBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "TwtWebViewController") as? TwtWebViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapInstagramBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "InstaWebViewController") as? InstaWebViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MainController: UITextFieldDelegate {
    
    
}

// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

