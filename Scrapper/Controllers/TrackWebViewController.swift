//
//  TrackWebViewController.swift
//  Scrapper
//
//  Created by Zaharia Nicolae on 07/04/2021.
//

import UIKit
import WebKit

class TrackWebViewController: UIViewController {

    @IBOutlet weak var webKitView: WKWebView!
    @IBOutlet weak var secondsLabel: UILabel!
    
    var baseURLString = "https://old.reddit.com"
    var urlPath = ""
    var totalSeconds = 10
    var passedSeconds = 0
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebKitView()
        loadLink(urlString: baseURLString + urlPath)
        setupNotifications()
    }
    
    deinit {
        // ViewController going away.  Kill the timer.
        stopTimer()
    }
    
    private func setupWebKitView() {
        webKitView.navigationDelegate = self
    }
    
    private func loadLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        webKitView.load(URLRequest(url: url))
        webKitView.allowsBackForwardNavigationGestures = true
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground),
                                               name:UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground),
                                               name:UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    private func startCountdown() {
        print("Timer Started")
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let `self` = self else { return }
            self.totalSeconds -= 1
            self.passedSeconds += 1
            self.updateSecondsLabel()
            if self.totalSeconds == 0 {
                PointsManager.shared.trackSiteComplete = true
            }
        }
    }
    
    private func stopTimer() {
        print("Timer Stopped")
        timer?.invalidate()
    }
    
    @objc func didEnterBackground(notification : NSNotification) {
        stopTimer()
    }
    
    @objc func didEnterForeground(notification : NSNotification) {
        startCountdown()
    }
    

    private func updateSecondsLabel() {
        secondsLabel.text = "\(passedSeconds) seconds passed"
    }
}

//MARK:- WKNavigationDelegate Methods
extension TrackWebViewController: WKNavigationDelegate, UIWebViewDelegate {
    
    //Equivalent of shouldStartLoadWithRequest:
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        guard let url = navigationAction.request.url else { return }
        print("decidePolicyFor - url: \(url)")
        if url.absoluteString.hasPrefix(baseURLString) {
            decisionHandler(.allow)
        } else {
            decisionHandler(.cancel)
            print("User Stoped leaving site")
        }
    }
    
    //Equivalent of webViewDidStartLoad:
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("didStartProvisionalNavigation - webView.url: \(String(describing: webView.url?.description))")
    }
    
    //Equivalent of didFailLoadWithError:
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        let nserror = error as NSError
        if nserror.code != NSURLErrorCancelled {
            webView.loadHTMLString("Page Not Found", baseURL: webView.url)
        }
    }
    
    //Equivalent of webViewDidFinishLoad:
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinish - webView.url: \(String(describing: webView.url?.description))")
        if !(timer?.isValid ?? false) {
            startCountdown()
        }
    }
    
}
    


